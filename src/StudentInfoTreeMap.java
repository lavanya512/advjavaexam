import java.util.TreeMap;

public class StudentInfoTreeMap {

	public static void main(String[] args) {
		TreeMap<String, String> tm = new TreeMap<String, String>();
        //add key-value pair to TreeMap
        tm.put("Reg No: 1", "Student 1");
        tm.put("Reg No: 2", "Student 2");
        tm.put("Reg No: 3", "Student 3");
        tm.put("Reg No: 4", "Student 4");
        tm.put("Reg No: 5", "Student 5");
		
        System.out.println(tm);
        System.out.println("Size of the TreeMap: "+tm.size());
	}

}
